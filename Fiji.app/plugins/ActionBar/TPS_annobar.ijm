// Action Bar description file : TPS_annobar.ijm
//run("Action Bar", getDirectory("plugins")+"/ActionBar/TPS_annobar.ijm");

<startupAction>
print("Started TPS_annobar.0.7");
fs=File.separator;
call("ij.Prefs.set", "tcf.anno.query.ijm",  getDirectory("macros")+"TPS-toolbar/dbAccess.ijm");
call("ij.Prefs.set", "tcf.anno.query.ijm",  getDirectory("macros")+"TPS-toolbar/resetDbAccess.ijm");
call("ij.Prefs.set", "tcf.anno.all.ijm",    getDirectory("macros")+"TPS-annobar/anno.all.ijm");
call("ij.Prefs.set", "tcf.anno.disp.ijm",    getDirectory("macros")+"TPS-annobar/anno.disp.ijm");
call("ij.Prefs.set", "tcf.anno.update.ijm", getDirectory("macros")+"TPS-annobar/anno.update.ijm");
call("ij.Prefs.set", "tcf.onto.show.ijm",   getDirectory("macros")+"TPS-annobar/onto.show.ijm");
call("ij.Prefs.set", "tcf.onto.load.ijm",   getDirectory("macros")+"TPS-annobar/onto.load.ijm");
call("ij.Prefs.set", "tcf.onto.reset.ijm",  getDirectory("macros")+"TPS-annobar/onto.reset.ijm");
call("ij.Prefs.set", "tcf.onto.save.ijm",   getDirectory("macros")+"TPS-annobar/onto.save.ijm");
call("ij.Prefs.set", "tcf.roi.add.ijm",     getDirectory("macros")+"TPS-annobar/roi.add.ijm");
call("ij.Prefs.set", "tcf.roi.load.ijm",    getDirectory("macros")+"TPS-annobar/roi.load.ijm");
call("ij.Prefs.set", "tcf.roi.save.ijm",    getDirectory("macros")+"TPS-annobar/roi.save.ijm");
call("ij.Prefs.set", "tcf.roi.show.ijm",    getDirectory("macros")+"TPS-annobar/roi.show.ijm");
call("ij.Prefs.set", "tcf.disp.tool", "annobar")

// define output directory for private annotations
ANNODIR=getDirectory("home")+"annotations"+fs;
call("ij.Prefs.set", "tcf.anno.config.annoPriv", ANNODIR);
File.makeDirectory(ANNODIR);

// define output directory for public annotations empty, so we can react properly.
call("ij.Prefs.set", "tcf.anno.config.annoPub", "@");
// TODO: this is not good, yet. lose end, which needs fixing.

// define output directory for annotation database
DBDIR=getDirectory("home")+"annotations"+fs+"db"+fs;
call("ij.Prefs.set", "tcf.anno.config.dbDir", DBDIR);
File.makeDirectory(DBDIR);

// define column separator character
call("ij.Prefs.set", "tcf.anno.config.csc", "#");
exit();
</startupAction>

<noGrid>
<text> private annotations
<line>

<button>
label=freeform
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
runMacro(call("ij.Prefs.get", "tcf.anno.all.ijm", "@"))
</macro>

<separator>

<button>
label=show
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
runMacro(call("ij.Prefs.get", "tcf.anno.disp.ijm", "@"))
</macro>

<separator>

<button>
label=update
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
call("ij.Prefs.set", "tcf.disp.tool", "annobar")
runMacro(call("ij.Prefs.get", "tcf.anno.update.ijm", "@"))
</macro>

<button>
label=query
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
Q=call("ij.Prefs.get", "tcf.anno.query.ijm", "@")
runMacro(Q, "annobar")
</macro>

</line>

<line>

<button>
label=ontology
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
runMacro(call("ij.Prefs.get", "tcf.anno.all.ijm", "@"))
</macro>

<separator>

<button>
label=show
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
runMacro(call("ij.Prefs.get", "tcf.anno.disp.ijm", "@"))
</macro>

<separator>

<button>
label=update
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
call("ij.Prefs.set", "tcf.disp.tool", "annobar")
runMacro(call("ij.Prefs.get", "tcf.anno.update.ijm", "@"))
</macro>

<button>
label=query
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
Q=call("ij.Prefs.get", "tcf.anno.query.ijm", "@")
runMacro(Q, "annobar")
</macro>

</line>

<text> private ontology ...

<line>

<button>
label=show
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
runMacro(call("ij.Prefs.get", "tcf.onto.show.ijm", "@"))
</macro>

<button>
label=reset
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
runMacro(call("ij.Prefs.get", "tcf.onto.reset.ijm", "@"))
</macro>

<button>
label=save
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
runMacro(call("ij.Prefs.get", "tcf.onto.save.ijm", "@"))
</macro>

<button>
label=load
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "private")
runMacro(call("ij.Prefs.get", "tcf.onto.load.ijm", "@"))
</macro>
</line>

<text>

<text> public annotations
<line>

<button>
label=freeform
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
runMacro(call("ij.Prefs.get", "tcf.anno.all.ijm", "@"))
</macro>

<separator>

<button>
label=show
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
runMacro(call("ij.Prefs.get", "tcf.anno.disp.ijm", "@"))
</macro>

<separator>

<button>
label=update
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
call("ij.Prefs.set", "tcf.disp.tool", "annobar")
runMacro(call("ij.Prefs.get", "tcf.anno.update.ijm", "@"))
</macro>

<button>
label=query
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "freeform")
Q=call("ij.Prefs.get", "tcf.anno.query.ijm", "@")
runMacro(Q, "annobar")
</macro>

</line>

<line>

<button>
label=ontology
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
runMacro(call("ij.Prefs.get", "tcf.anno.all.ijm", "@"))
</macro>

<separator>

<button>
label=show
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
runMacro(call("ij.Prefs.get", "tcf.anno.disp.ijm", "@"))
</macro>

<separator>

<button>
label=update
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
call("ij.Prefs.set", "tcf.disp.tool", "annobar")
runMacro(call("ij.Prefs.get", "tcf.anno.update.ijm", "@"))
</macro>

<button>
label=query
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
call("ij.Prefs.set", "tcf.anno.config.annoMode", "ontology")
Q=call("ij.Prefs.get", "tcf.anno.query.ijm", "@")
runMacro(Q, "annobar")
</macro>

</line>

<text> public ontology ...

<line>

<button>
label=show
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
runMacro(call("ij.Prefs.get", "tcf.onto.show.ijm", "@"))
</macro>

<button>
label=reset
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
runMacro(call("ij.Prefs.get", "tcf.onto.reset.ijm", "@"))
</macro>

<button>
label=save
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
runMacro(call("ij.Prefs.get", "tcf.onto.save.ijm", "@"))
</macro>

<button>
label=load
icon=noicon
arg=<macro>
call("ij.Prefs.set", "tcf.anno.config.annoType", "public")
runMacro(call("ij.Prefs.get", "tcf.onto.load.ijm", "@"))
</macro>
</line>
</noGrid>
// end of file
