// get annoType for annotation (private vs. public)
annoType=call("ij.Prefs.get", "tcf.anno.config.annoType", "@");
//print("onto.show: "+annoType);
if (annoType == "@" ){
	exit("onto.show: annoType is not defined!");
}
// get annoMode for annotation (freeform vs. ontology)
annoMode=call("ij.Prefs.get", "tcf.anno.config.annoMode", "@");
//print("onto.show: "+annoMode);
if (annoMode == "@" ){
	exit("onto.show: annoMode is not defined!");
}
print("onto.show: ", annoType, annoMode);

O=call("ij.Prefs.get", "tcf.anno."+annoType+"."+annoMode+".onto", "@");
P=replace(O, "|","\n");
print("------------------------------------");
print("Your current "+annoType+" ontology:");
print(P);
print("------------------------------------");
selectWindow("Log");