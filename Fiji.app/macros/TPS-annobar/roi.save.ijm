//ROI=getArgument();
ROI=call("ij.Prefs.get", "tcf.anno.config.roi.current", "@");
print("roi.save: "+ROI);

// get annoType for annotation (private vs. public)
annoType=call("ij.Prefs.get", "tcf.anno.config.annoType", "@");
print(annoType);
if (annoType == "@" ){
	exit("roi.save: annoType is not defined!");
}
// get annoMode for annotation (freeform vs. ontology)
annoMode=call("ij.Prefs.get", "tcf.anno.config.annoMode", "@");
//print("free: "+annoMode);
if (annoMode == "@" ){
	exit("roi.save: annoMode is not defined!");
}

autosave=call("ij.Prefs.get", "tcf.anno.config."+annoType+"."+annoMode+".autosave", 1);
if ( autosave == 1 ){
	roiManager("Save", ROI);
}