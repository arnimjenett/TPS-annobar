// get annoType for annotation (private vs. public)
annoType=call("ij.Prefs.get", "tcf.anno.config.annoType", "@");
//print("onto.load: "+annoType);
if (annoType == "@" ){
	exit("onto.load: annoType is not defined!");
}
// get annoMode for annotation (freeform vs. ontology)
annoMode=call("ij.Prefs.get", "tcf.anno.config.annoMode", "@");
//print("onto.load: "+annoMode);
if (annoMode == "@" ){
	exit("onto.load: annoMode is not defined!");
}
print("onto.load: ", annoType, annoMode);

ONTO=File.openAsString("");
call("ij.Prefs.set", "tcf.anno."+annoType+"."+annoMode+".onto", ONTO);
