// set global variables for coordinates of image location
var imgX=call("ij.Prefs.get", "tcf.anno.config.imageX", "10");
var imgY=call("ij.Prefs.get", "tcf.anno.config.imageY", "10");

// get annoType for annotation (private vs. public)
annoType=call("ij.Prefs.get", "tcf.anno.config.annoType", "@");
//print("anno.all: "+annoType);
if (annoType == "@" ){
	exit("anno.all: annoType is not defined!");
}
// get annoMode for annotation (freeform vs. ontology)
annoMode=call("ij.Prefs.get", "tcf.anno.config.annoMode", "@");
//print("anno.all: "+annoMode);
if (annoMode == "@" ){
	exit("anno.all: annoMode is not defined!");
}
print("anno.all: ", annoType, annoMode);
// get column-separator
csc=call("ij.Prefs.get", "tcf.anno.config.csc", "@");
autoClose=call("ij.Prefs.get", "tcf.disp.autoClose", "@");
autoProgress=call("ij.Prefs.get", "tcf.disp.autoProgress", "@");
//print("column-separator: "+csc);
if (csc == "@" ){
	exit("config.csc: column-separation-character is not defined!");
} else {
// define column-separator for each column.
	nof=10;
	cs=newArray(nof);
	for (i=0; i<nof; i++){
		cs[i]=csc+i+csc;
	}
//	Array.print(cs);
}
// create shortcut for file-sepatator
// (the use of File.separator is needed due to the fact, that windows is using a back-slash in some cases)
fs=File.separator;
// build timestamp
getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
D=substring(year,2,4)+IJ.pad(month+1, 2)+IJ.pad(dayOfMonth,2); //short time stamp: yymmdd
TS=substring(year,2,4)+IJ.pad(month+1, 2)+IJ.pad(dayOfMonth,2)+IJ.pad(hour,2)+IJ.pad(minute,2)+IJ.pad(second,2); // long time stamp: yymmddhhmmss
// get name and basename of active image
if (nImages == 0){
	exit();
}
title=getTitle();
IID=getImageID();
selectImage(IID);
setImageLocation(IID);
dn=getInfo("image.directory");
fn=getInfo("image.filename");
imagePath=dn+fn;
bn=split(fn, ".");
bn=bn[0];
// get user - for annotations
HOME=getDirectory("home");
USER=split(HOME, fs);
USER=USER[lengthOf(USER)-1];
// define output directory for annotation corresponding to annoType
// privte annotations are stored in a folder 'annotations' in the user's home directory.
// public annotations are stored in the folder the images is loaded from.
if (matches(annoType, "private")) {
	ANNODIR=call("ij.Prefs.get", "tcf.anno.config.annoPriv", "@");
} else {
	ANNODIR=getDirectory("image");
}
ANNO=ANNODIR+bn+"."+USER+"."+annoType+"."+annoMode+".anno";
print(ANNO);
// define location in which annotation data base will be build
DBDIR=call("ij.Prefs.get", "tcf.anno.config.dbDir", "@");
// define output for segementation based anotations
//ROI=ANNODIR+fs+D+"."+bn+"."+USER+"."+annoType+"."+annoMode+".roi.zip";
ROI=ANNODIR+fs+bn+"."+USER+"."+annoType+"."+annoMode+".roi.zip";
call("ij.Prefs.set", "tcf.anno.config.roi.current", ROI);
runMacro(call("ij.Prefs.get", "tcf.roi.load.ijm", "@"), ROI); //TODO: something wrong here, fix.


// create dialog to get annotation value
Dialog.createNonBlocking("TCF "+annoType+" "+annoMode+" annotator");
Dialog.addMessage(title);
Dialog.addString("annotation:", "");
autosave=call("ij.Prefs.get", "tcf.anno.config."+annoType+"."+annoMode+".autosave", 1);
Dialog.addCheckbox("Autosave "+annoType+" "+annoMode+" ROI", autosave);
Dialog.addCheckbox("AutoClose", autoClose);
Dialog.addCheckbox("AutoProgress", autoProgress);
if (annoMode == "ontology"){
	O=call("ij.Prefs.get", "tcf.anno."+annoType+"."+annoMode+".onto", "@");
	if (O == "@") {
		ONTO=newArray(1);
		ontology=0;
	} else {
		instring=call("ij.Prefs.get", "tcf.anno."+annoType+"."+annoMode+".onto", "");
		ONTO=split(instring, "|");
		ONTO=Array.sort(ONTO);
		ontology=1;
	}
	i=lengthOf(ONTO);
	y=-floor(sqrt(i));
	x=floor(i/y);
	columns=y*-1;
	rows=x*-1;
	defaults=newArray(lengthOf(ONTO));
	if(ontology == 1){
		Dialog.addCheckboxGroup(rows, columns, ONTO, defaults);
	}
}
//Dialog.addMessage(annostring);
Dialog.show();
// get new annotation term from dialog
string=Dialog.getString();
// write new annotation term to annotation file
annoString=makeAnnoString(string);
File.append(annoString, ANNO);
// build list of terms to select label of ROI from
//print("los: ",lengthOf(string));
ROIstring=string;
autosave=Dialog.getCheckbox();
call("ij.Prefs.set", "tcf.anno.config."+annoType+"."+annoMode+".autosave", autosave);
autoClose=Dialog.getCheckbox();
call("ij.Prefs.set", "tcf.disp.autoClose", autoClose);
autoProgress=Dialog.getCheckbox();
call("ij.Prefs.set", "tcf.disp.autoProgress", autoProgress);
// store current image location for the placing the next one at the same location
selectImage(IID);
getLocationAndSize(x, y, width, height);
call("ij.Prefs.set", "tcf.anno.config.imageX", x);
call("ij.Prefs.set", "tcf.anno.config.imageY", y);

if (annoMode == "ontology"){
// write ontology based annotation to annotation file
	if(ontology == 1){
		for (i=0; i<lengthOf(ONTO); i++) {
			if (Dialog.getCheckbox() == 1) {
				annoString=makeAnnoString(ONTO[i]);
				File.append(annoString, ANNO);
				print(ONTO[i]);
				ROIstring=ROIstring+"|"+ONTO[i]; // adding ontology terms to list of labels for the ROI
			}
		}
	}
// exporting new annotation term into ontology
// ij.Prefs is accepting only one string, so we need to concat the terms
	outArray=Array.concat(ONTO, string);
	outString="";
	for (i=0; i<lengthOf(outArray); i++){
		if(ontology == 1){
			outString=outString+"|"+outArray[i];
		} else {
			outString=outArray[i];
		}
	}
	print(outString);
	call("ij.Prefs.set", "tcf.anno."+annoType+"."+annoMode+".onto", outString);
}
ROIlabels=split(ROIstring, "|");
if (ROIlabels[0] == 0){
	ROIlabels=Array.slice(ROIlabels,1,lengthOf(ROIlabels));
}
Array.print(ROIlabels);
if (actSel() == 0 ){
	call("ij.Prefs.set", "tcf.anno.config.roi.selection", 0);
} else {
	if (lengthOf(ROIlabels) > 1) {
		Dialog.create("Select label for ROI");
		rsdef=newArray(lengthOf(ROIlabels));
		Dialog.addRadioButtonGroup("label of ROI", ROIlabels, lengthOf(ROIlabels), 1, ROIlabels[0]);
		Dialog.show();
		label=Dialog.getRadioButton();
	} else {
		label=ROIlabels[0];
	}
	runMacro(call("ij.Prefs.get", "tcf.roi.add.ijm", "@"), label);
	call("ij.Prefs.set", "tcf.anno.config.roi.selection", 1);
}

if (autoClose) {
	selectImage(IID);
	close();
}
if (autoProgress) {
	runMacro(call("ij.Prefs.get", "tcf.anno.all.ijm", "@"))
}

function setImageLocation(id){
	selectImage(IID);
	getDimensions(width, height, channels, slices, frames);
	imgX=call("ij.Prefs.get", "tcf.anno.config.imageX", "10");
	imgY=call("ij.Prefs.get", "tcf.anno.config.imageY", "10");
	setLocation(imgX, imgY, width, height);
	while ( getZoom() < 1) {
		run("In [+]");
	} 
	while ( getZoom() > 1) {
		run("Out [-]");
	} 
	run("Out [-]");
//	run("In [+]");
}	

function actSel(){
	// detect and annotate active selection
	getDimensions(dw, dh, channels, slices, frames);
	getSelectionBounds(x, y, bw, bh);
	if (dw == bw && dw ==bw ){
//		print("no selection");
		return 0;
	} else {
		return 1;
	}
}

function makeAnnoString(ontoTerm) {
//	annoString=ontoTerm+"\t"+title+"\t"+USER+"\t"+TS;
//	annoString=cs[0]+"\t"+ontoTerm+"\t"+cs[1]+"\t"+title+"\t"+cs[2]+"\t"+USER+"\t"+cs[3]+"\t"+TS+"\t"+cs[4];
	annoString=cs[0]+"\t"+ontoTerm+"\t"+cs[1]+"\t"+imagePath+"\t"+cs[2]+"\t"+USER+"\t"+cs[3]+"\t"+TS+"\t"+cs[4];
//	annoString=cs[0]+"\t"+ontoTerm+"\t"+cs[1]+"\t"+bn+"\t"+cs[2]+"\t"+USER+"\t"+cs[3]+"\t"+TS+"\t"+cs[4];
	return annoString;
}