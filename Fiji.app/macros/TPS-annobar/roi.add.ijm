annoVal=getArgument();
roiManager("Add");
current=roiManager("count")-1;
roiManager("Select", current);
roiManager("Rename", annoVal);
roiManager("Set Color", "red");
roiManager("Set Line Width", 3);
roiManager("Update");
runMacro(call("ij.Prefs.get", "tcf.roi.save.ijm", "@"));
runMacro(call("ij.Prefs.get", "tcf.roi.show.ijm", "@"));

