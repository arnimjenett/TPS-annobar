#!/bin/bash
#TODO: optionally skip the TMP-creation
TMP=~/TCFanno.tmp
cat $1 |grep -E "$2|$3|$4" > $TMP
#wc -l $TMP
for i in $(sed -e 's@.*#1#[[:blank:]]@@' -e 's@[[:blank:]]#2#.*@@' $TMP |sort -u); do
   printf "$i	"
   grep $i $TMP |sed -e 's@.*#0#[[:blank:]]@@' -e 's@[[:blank:]]#1#.*@@' |sort -u |tr "\n" ","
   printf "\n"
done |awk "/$2/ && /$3/ && /$4/ {print}" |cut -f 1
