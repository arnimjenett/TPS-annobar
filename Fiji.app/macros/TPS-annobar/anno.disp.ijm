// get annoType for annotation (private vs. public)
annoType=call("ij.Prefs.get", "tcf.anno.config.annoType", "@");
//print("anno.disp: "+annoType);
if (annoType == "@" ){
	exit("anno.disp: annoType is not defined!");
}
// get annoMode for annotation (freeform vs. ontology)
annoMode=call("ij.Prefs.get", "tcf.anno.config.annoMode", "@");
//print("anno.disp: "+annoMode);
if (annoMode == "@" ){
	exit("anno.disp: annoMode is not defined!");
}
print("anno.disp: ", annoType, annoMode);

// create shortcut for file-sepatator 
// (the use of File.separator is needed due to the fact, that windows is using a back-slash in some cases)
fs=File.separator;
// get basename of active image
if (nImages > 0 ){
	fn=getInfo("image.filename");
} else {
	exit("no images open.");
}
bn=split(fn, ".");
bn=bn[0];
// get user - for annotations
HOME=getDirectory("home");
USER=split(HOME, fs);
USER=USER[lengthOf(USER)-1];
// define output directory for annotation corresponding to annoType
// privte annotations are stored in a folder 'annotations' in the user's home directory.
// public annotations are stored in the folder the images is loaded from.
if (matches(annoType, "private")) {
	ANNODIR=call("ij.Prefs.get", "tcf.anno.config.annoPriv", "@");
} else {
	ANNODIR=getDirectory("image");
}
ANNO=ANNODIR+bn+"."+USER+"."+annoType+"."+annoMode+".anno";
if ( File.exists(ANNO) == 1 ){
	print("\\Clear");
// display annotation 
	print(ANNO);
	anno=File.openAsString(ANNO);
	print(replace(anno, "#.#", "   "));
	anno=split(anno, "\n");
	annotation=newArray(lengthOf(anno));
	image=newArray(lengthOf(anno));
	user=newArray(lengthOf(anno));
	timestamp=newArray(lengthOf(anno));
	for (a=0; a<lengthOf(anno);a++){
		entries=split(replace(anno[a], "#.#", "@"), "@");
		for (j=0; j<lengthOf(entries); j++){
			print(j,entries[j]);
		}
		annotation[a]=replace(entries[0],"\t","");
		image[a]=replace(entries[1],"\t","");
		user[a]=replace(entries[2],"\t","");
		timestamp[a]=replace(entries[3],"\t","");
	}
	Array.show(ANNO, annotation, image, user, timestamp);
// define output for segementation based anotations
	ROI=ANNODIR+fs+bn+"."+USER+"."+annoType+"."+annoMode+".roi.zip";
	call("ij.Prefs.set", "tcf.anno.config.roi.current", ROI);
// load and show ROI collection
	runMacro(call("ij.Prefs.get", "tcf.roi.load.ijm", "@"), ROI);
	runMacro(call("ij.Prefs.get", "tcf.roi.show.ijm", "@"), ROI);
} else { 
	print("No "+annoType+" "+annoMode+" annotation available.");
}

