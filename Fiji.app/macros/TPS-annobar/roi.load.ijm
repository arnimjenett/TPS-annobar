ROI=call("ij.Prefs.get", "tcf.anno.config.roi.current", "@");
	bn=split(getTitle(),".");
	bn=bn[0];
//	ROI="C:\\Users\\jenett\\annotations\\161106."+bn+".jenett.private.freeform.roi.zip";

// if autosave is on always load pre-existing ROI, because otherwise it will be overwritten

// check for image identity - switching roi-sets between images
lastROI=call("ij.Prefs.get", "tcf.anno.config.roi.last", "@");
if (ROI != lastROI ||  roiManager("index") == -1 ){
	roiManager("reset");
	if (File.exists(ROI) == 1) {
		roiManager("Open", ROI);
	}
	call("ij.Prefs.set", "tcf.anno.config.roi.last", ROI);
}
runMacro(call("ij.Prefs.get", "tcf.roi.show.ijm", "@"));
// check for active selection