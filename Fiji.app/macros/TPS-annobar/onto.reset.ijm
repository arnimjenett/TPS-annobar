// get annoType for annotation (private vs. public)
annoType=call("ij.Prefs.get", "tcf.anno.config.annoType", "@");
//print("onto.reset: "+annoType);
if (annoType == "@" ){
	exit("onto.reset: annoType is not defined!");
}
// get annoMode for annotation (freeform vs. ontology)
annoMode=call("ij.Prefs.get", "tcf.anno.config.annoMode", "@");
//print("onto.reset: "+annoMode);
if (annoMode == "@" ){
	exit("onto.reset: annoMode is not defined!");
}
print("onto.reset: ", annoType, annoMode);

call("ij.Prefs.set", "tcf.anno."+annoType+"."+annoMode+".onto", "@");
print(annoType+" ontology was reset.");