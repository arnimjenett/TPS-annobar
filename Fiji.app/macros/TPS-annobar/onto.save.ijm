// get annoType for annotation (private vs. public)
annoType=call("ij.Prefs.get", "tcf.anno.config.annoType", "@");
print("onto.save: "+annoType);
if (annoType == "@" ){
	exit("anno.save: annoType is not defined!");
}
// get annoMode for annotation (freeform vs. ontology)
annoMode=call("ij.Prefs.get", "tcf.anno.config.annoMode", "@");
//print("onto.show: "+annoMode);
if (annoMode == "@" ){
	exit("onto.show: annoMode is not defined!");
}
print("onto.show: ", annoType, annoMode);

O=call("ij.Prefs.get", "tcf.anno."+annoType+"."+annoMode+".onto", "@");

print(O);
if (O == "@") {
		exit("Current ontology has nothing to save.");
} else{
	ontoName=getString("Define ontology name", "standard");
	if(matches(annoType, "private")) {
//		ONTOFILE=getDirectory("home")+ontoName+"."+annoType+".onto.txt";
		ONTODIR=call("ij.Prefs.get", "tcf.anno.config.dbDir", "@");
		ONTOFILE=ONTODIR+ontoName+"."+annoType+".onto";
	} else{
		ANNODIR=getDirectory("Select directory for ontology");
		ONTOFILE=ANNODIR+ontoName+"."+annoType+".onto";
	}
	OF=File.open(ONTOFILE);
	print(OF, O);
	File.close(OF);
	print("Ontology written to ");
	print(ONTOFILE);
}