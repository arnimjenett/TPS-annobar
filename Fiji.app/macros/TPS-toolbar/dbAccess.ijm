param=getArgument();
//if(lengthOf(param) > 0) {
//	paramArray=split(param, " "); //deprecated but potentially useful
//	closeAll=paramArray[0];
//}
// taken preferences; avoid these:
//	call("ij.Prefs.set", "tcf.disp."+tool+".modus.mip", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".modus.aip", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".modus.cs", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".modus.mp4", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".opt.mipopt", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".opt.aipopt", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".opt.csopt", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".opt.clahe", 0);

//	call("ij.Prefs.set", "tcf.disp."+tool+".autoClose", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".repeatMovie", 0);
//	call("ij.Prefs.set", "tcf.disp."+tool+".movieRes", 0);

//	call("ij.Prefs.set", "tcf.disp."+tool+".SS1", "@");
//	call("ij.Prefs.set", "tcf.disp."+tool+".SS2", "@");
//	call("ij.Prefs.set", "tcf.disp."+tool+".SS3", "@");
//	call("ij.Prefs.set", "tcf.disp."+tool+".BO1", "OR");
//	call("ij.Prefs.set", "tcf.disp."+tool+".BO2", "OR");

//	call("ij.Prefs.set", "tcf.disp."+tool+".lif", 0)

// define which tool (annobar or toolbar) called this script
	tool=call("ij.Prefs.get", "tcf.disp.tool", "toolbar");
	print(tool);

fs=File.separator;

var indexPath="";
var os="";
var ua="";
var	dl="#";
// define stackypes	
stackTypes=newArray("lif", "nd2", "czi", "png"); //TODO: read from fsdb scripts.config


// on the way to the integration into the fsdb
fsdb=0
if (fsdb == 0){
	DATAROOT="/DATA";
	LAB="TPS";
	LABDIR=DATAROOT+"/"+LAB;
	LABDATADIR=LABDIR+"/labdata";
} else {
	runMacro("fsdb.config.ijm");
	//TODO: develop this branch
}

if ( call("ij.Prefs.get", "tcf.disp."+tool+".closeAll", 1)  == 1 ){
	run("Close All");
}

// run functions
getOS(); 
dialog();

if (nImages > 1 ){
	run("Tile");
}

function getOS() {
	OS=getInfo("os.name");
	if ( startsWith(OS, "Linux") == 1){
		os="l";
	} else if ( startsWith(OS, "Windows") == 1) {
		os="w";
	} else {
		print "Can not verify OS.");
	}
}

function dialog(){
// define secDataTypes (modi)
	dataFormats=newArray("mip", "aip", "cs", "mp4", "org"); //TODO: read from fsdb scripts.config ??
// define optimized data formats
	optFormats=newArray("sc", "oc", "cl"); //TODO: read from fsdb scripts.config ????!
// define boolean terms
	BOOLEANS=newArray("---", "AND", "OR", "NOT");
	//booleans=newArray("&&", "||", "NOT");
// define storage of query terms
	SS1=call("ij.Prefs.get", "tcf.disp."+tool+".SS1", "");
	SS2=call("ij.Prefs.get", "tcf.disp."+tool+".SS2", "");
	SS3=call("ij.Prefs.get", "tcf.disp."+tool+".SS3", "");
	//BOO=call("ij.Prefs.get", "tcf.disp."+tool+".BOO", "");
	BO1=call("ij.Prefs.get", "tcf.disp."+tool+".BO1", "OR");
	BO2=call("ij.Prefs.get", "tcf.disp."+tool+".BO2", "OR");	

// populate defaultValues file types with zero or previous value
	stackTypeDefaults=newArray(lengthOf(stackTypes));
	for (i=0; i<lengthOf(stackTypes); i++){
		stackTypeDefaults[i]=call("ij.Prefs.get", "tcf.disp."+tool+"."+stackTypes[i], 0);
		print("tcf.disp."+tool+"."+stackTypes[i]+": "+stackTypeDefaults[i]);
	}
	stRows=1;
	stColumns=lengthOf(stackTypes);
// populate defaultValues of secData with zero or previous value
	formatDefaults=newArray(lengthOf(dataFormats));
	for (i=0; i<lengthOf(dataFormats); i++){
		formatDefaults[i]=call("ij.Prefs.get", "tcf.disp."+tool+".modus."+dataFormats[i], 0);
		print("tcf.disp."+tool+".modus."+dataFormats[i]+": "+formatDefaults[i]);
	}
// populate defaultValues of optimized secData with zero or previous value
	optDefaults=newArray(lengthOf(optFormats));
	for (i=0; i<lengthOf(optFormats); i++){
		optDefaults[i]=call("ij.Prefs.get", "tcf.disp."+tool+".opt."+optFormats[i], 0);
		print("tcf.disp."+tool+".opt."+optFormats[i]+": "+optDefaults[i]);
	}
// define dialog layout	
	rows=1;
	n=lengthOf(dataFormats);
	//Array.print(dataFormats);
	//Array.print(formatDefaults);
// create dialog	
	Dialog.create("TPS-"+tool+"-query");
	if (tool == "annobar") {
		annoTypes=newArray("private", "public");
		annoType=call("ij.Prefs.get", "tcf.disp."+tool+".annoType", "private");
		annoModes=newArray("freeform", "ontology");
		annoMode=call("ij.Prefs.get", "tcf.disp."+tool+".annoMode", "freeform");
		Dialog.addMessage("query "+annoType+" "+annoMode+" annotations");
	}
	Dialog.addMessage("define up to three search terms below:"); 
	Dialog.addString("search term 1", SS1);
	Dialog.addChoice("boolean 1", BOOLEANS, BO1);
	Dialog.addString("search term 2", SS2);
	Dialog.addChoice("boolean 2", BOOLEANS, BO2);
	Dialog.addString("search term 3", SS3);
	Dialog.addMessage("select input file type(s):"); 
	Dialog.addCheckboxGroup(stRows, stColumns, stackTypes, stackTypeDefaults);
	Dialog.addMessage("select output file type:");
	Dialog.addCheckboxGroup(rows, lengthOf(dataFormats), dataFormats, formatDefaults);
	Dialog.addCheckboxGroup(rows, lengthOf(optFormats), optFormats, optDefaults);
	if (tool == "annobar") {
		Dialog.addRadioButtonGroup("annotation scope", annoTypes, 1, 2, annoType);
		Dialog.addRadioButtonGroup("annotation form", annoModes, 1, 2, annoMode);
	}
	movieRess=newArray("high", "low");
	movieRes=call("ij.Prefs.get", "tcf.disp."+tool+".annoMode", "high");
	Dialog.addRadioButtonGroup("movie resolution", movieRess, 1, 2, movieRes);
	Dialog.addCheckbox("repeat movie", 0);
	Dialog.addCheckbox("close open images before loading", 1);
	Dialog.addCheckbox("close after annotation", 0);
// display dialog
	Dialog.show();
	print("---");
//get data from dialog: search terms and booleans
	SS1=Dialog.getString();
	call("ij.Prefs.set", "tcf.disp."+tool+".SS1", SS1);
	BO1=Dialog.getChoice();
	call("ij.Prefs.set", "tcf.disp."+tool+".BO1", BO1);
	SS2=Dialog.getString();
	call("ij.Prefs.set", "tcf.disp."+tool+".SS2", SS2);
	BO2=Dialog.getChoice();
	call("ij.Prefs.set", "tcf.disp."+tool+".BO2", BO2);
	SS3=Dialog.getString();
	call("ij.Prefs.set", "tcf.disp."+tool+".SS3", SS3);
	print("---------\nData for "+SS1+" "+BO1+" "+SS2+" "+BO2+" "+SS3+"\n---------");
//get data from dialog: checkbox-groups
	coll="";
	typ="";	
	for (i=0; i<lengthOf(stackTypes); i++) {
		st=Dialog.getCheckbox();
		call("ij.Prefs.set", "tcf.disp."+tool+"."+stackTypes[i], st);
		if (st == 1) {
			stacktype=stackTypes[i];
			//print(stacktype+":");
			typ=typ+dl+stacktype;
		}
	}
	for (i=0; i<lengthOf(dataFormats); i++) {
		df=Dialog.getCheckbox();
		call("ij.Prefs.set", "tcf.disp."+tool+".modus."+dataFormats[i], df);
		if (df == 1) {
			modus=dataFormats[i];
			//print(modus+":");
			coll=coll+dl+modus;
		}
	}
	for (i=0; i<lengthOf(optFormats); i++) {
		df=Dialog.getCheckbox();
		call("ij.Prefs.set", "tcf.disp."+tool+".opt."+optFormats[i], df);
		if (df == 1) {
			modus=optFormats[i];
			//print(modus+":");
			coll=coll+dl+modus;
		}
	}
	if (tool == "annobar") {
		annoType=Dialog.getRadioButton();
		call("ij.Prefs.set", "tcf.disp."+tool+".annoType", annoType);
		annoMode=Dialog.getRadioButton();
		call("ij.Prefs.set", "tcf.disp."+tool+".annoMode", annoMode);
	}
//get data from dialog: individual radio-buttons and checkboxes
	movieRes=Dialog.getRadioButton();
	call("ij.Prefs.set", "tcf.disp."+tool+".movieRes", movieRes);
	repeatMovie=Dialog.getCheckbox();
	call("ij.Prefs.set", "tcf.disp."+tool+".repeatMovie", repeatMovie);
	closeAllBeforeLoading=Dialog.getCheckbox();
	call("ij.Prefs.set", "tcf.disp."+tool+".closeAll", closeAllBeforeLoading);
	autoClose=Dialog.getCheckbox();
	call("ij.Prefs.set", "tcf.disp."+tool+".autoClose", autoClose);
	//print(autoClose, movieRes, repeatMovie);
	modi=split(coll, dl);
	typi=split(typ, dl);
	if ( tool == "toolbar" ){
		for (j=0; j<lengthOf(typi);j++){
			for (i=0;i<lengthOf(modi);i++){
				print("Found "+modi[i]+" in "+ typi[j]);
				displayImages(modi[i],typi[j]);
			}
		}
	} else if ( tool == "annobar") {
		displayImages("bla", "blub");
	} else {
		exit("function (old) dialog: Can not identify tool: "+tool);
	}
	print("done.");
}

function displayImages(modus, stacktype){
	SS1=call("ij.Prefs.get", "tcf.disp."+tool+".SS1", "");
	SS2=call("ij.Prefs.get", "tcf.disp."+tool+".SS2", "");
	SS3=call("ij.Prefs.get", "tcf.disp."+tool+".SS3", "");
//	BOO=call("ij.Prefs.get", "tcf.disp."+tool+".BOO", "");
	BO1=call("ij.Prefs.get", "tcf.disp."+tool+".BO1", "");
	BO2=call("ij.Prefs.get", "tcf.disp."+tool+".BO2", "");
	if ( tool == "toolbar" ){
		indexPath=call("ij.Prefs.get", "tcf.disp."+tool+".indexPath", "@");
//		print("db", indexPath);
		if (indexPath == "@") {
			indexPath=getDirectory("select index directory");
		}
		call("ij.Prefs.set", "tcf.disp."+tool+".indexPath", indexPath);
		mountPoint=call("ij.Prefs.get", "tcf.disp."+tool+".mountPoint", "@");
//TODO: if mountPoint=="@" get it from getDirectory("temp")+"mipDisplay.mem.txt"
		mountPoint=replace(mountPoint, fs, "/");
//		index=indexPath+"*."+modus+".index";
		if (modus == "org"){
			index=indexPath+stacktype+".index"; //TODO: test with original data
		} else {
			index=indexPath+stacktype+"."+modus+".index"; 
		}
		print(index);
	} else if ( tool == "annobar"){
		// get path to HOME/annotations/db 
		indexPath=call("ij.Prefs.get", "tcf.anno.config.dbDir", "@");
//		print("db", indexPath);
		if (indexPath == "@") {
//			indexPath=getDirectory("select database directory");
			indexPath=getDirectory("home")+fs+"annotations/db";
			if (File.isDirectory(indexPath) == 0 ){
				File.makeDirectory(getDirectory("home")+fs+"annotations/");
				File.makeDirectory(getDirectory("home")+fs+"annotations/db");
			}
		}
		call("ij.Prefs.set", "tcf.anno.config.dbDir", indexPath);
		// get annotation type to be queried - private vs. public
		annoType=call("ij.Prefs.get", "tcf.disp."+tool+".annoType", "@");
		print(tool+".annoType: "+annoType);
		if (annoType == "@" ){
			exit("function displayImages: "+tool+".anno.query: annoType is not defined!");
		}
		// get anotation mode to be queried - freeform vs. ontology based
		annoMode=call("ij.Prefs.get", "tcf.disp."+tool+".annoMode", "@");
		print(tool+".annoMode: "+annoMode);
		if (annoMode == "@" ){
			exit("function displayImages: "+tool+".anno.query: annoMode is not defined!");
		}
		print(tool+".anno.query: "+annoType+" "+annoMode);
		index=indexPath+fs+"TCFanno."+annoType+"."+annoMode+".txt";
	} else {
		exit("function displayImages: "+"Can not resolve used tool: "+tool);	
	}
	if(File.exists(index)){ 
		FILELIST=File.openAsString(index);
	} else {
		exit("function displayImages: "+"Can't access index. \n Make sure that "+index+" exists.");
	}
	FILELIST=split(FILELIST, "\n");
	dummy="@@@@@";
	string="";
	for (l=0; l<lengthOf(FILELIST); l++){
		img="";
		if (lengthOf(SS1) == 0) {SS1 = dummy;}
		if (lengthOf(SS2) == 0) {SS2 = dummy; BO1 = "OR";}
		if (lengthOf(SS3) == 0) {SS3 = dummy; BO1 = "OR"; BO2 = "OR";}
		if (BO1 == "AND" && BO2 == "AND" ) {
			case="A";
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 && matches(FILELIST[l] ,".*"+SS2+".*") == 1 && matches(FILELIST[l] ,".*"+SS3+".*") == 1) {
				img=FILELIST[l];					
			}
		} else if (BO1 == "AND" && BO2 == "OR" ) {
			case="B";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 && matches(FILELIST[l] ,".*"+SS2+".*") == 1 || matches(FILELIST[l] ,".*"+SS1+".*") == 1 && matches(FILELIST[l] ,".*"+SS3+".*") == 1) {
				img=FILELIST[l];					
			}
		} else if (BO1 == "AND" && BO2 == "NOT" ) {
			case="C";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 && matches(FILELIST[l] ,".*"+SS2+".*") == 1 && matches(FILELIST[l] ,".*"+SS3+".*") == 0) {
				img=FILELIST[l];					
			}
		} else if (BO1 == "OR" && BO2 == "AND" ) {
			case="D";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 || matches(FILELIST[l] ,".*"+SS2+".*") == 1 && matches(FILELIST[l] ,".*"+SS3+".*") == 1) {
				img=FILELIST[l];					
			}	
		} else if (BO1 == "OR" && BO2 == "OR" ) {
			case="E";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 || matches(FILELIST[l] ,".*"+SS2+".*") == 1 || matches(FILELIST[l] ,".*"+SS3+".*") == 1) {
				img=FILELIST[l];					
			}
		} else if (BO1 == "OR" && BO2 == "NOT" ) {
			case="F";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 || matches(FILELIST[l] ,".*"+SS2+".*") == 1 && matches(FILELIST[l] ,".*"+SS3+".*") == 0) {
				img=FILELIST[l];					
			}
		} else if (BO1 == "NOT" && BO2 == "AND" ) {
			case="G";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 && matches(FILELIST[l] ,".*"+SS2+".*") == 0 && matches(FILELIST[l] ,".*"+SS3+".*") == 1) {
				img=FILELIST[l];					
			}
		} else if (BO1 == "NOT" && BO2 == "OR" ) {
			case="H";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 && matches(FILELIST[l] ,".*"+SS2+".*") == 0 || matches(FILELIST[l] ,".*"+SS3+".*") == 1) {
				img=FILELIST[l];					
			}
		} else if (BO1 == "NOT" && BO2 == "NOT" ) {
			case="I";			
			if (matches(FILELIST[l] ,".*"+SS1+".*") == 1 && matches(FILELIST[l] ,".*"+SS2+".*") == 0 && matches(FILELIST[l] ,".*"+SS3+".*") == 0) {
				img=FILELIST[l];					
			}
		} 
		//print(case); // for debugging only
// create a unique list of retrieved images
		if (lengthOf(img) > 0) {
			if (tool == "annobar") {
// isolate path to image from annotations
				img=replace(img, ".*#1#", "");
				img=replace(img, "#2#.*", "");
				img=replace(img, "\t", "");
				img=replace(img, "\\", "/");
			}
			string=uniqArray(string, img);
		}
	}
	if(lengthOf(ua) >15){
		Dialog.create("WARNING!");
		Dialog.addMessage("This query results in "+lengthOf(ua)+" results.\nAre you sure you want to load them all?");
		Dialog.addCheckbox("Display file list instead", 1);
		Dialog.show();
		displayFileList=Dialog.getCheckbox();
	} else {
		displayFileList=0;
	}
	for (i=0;i<lengthOf(ua);i++){
		img=ua[i];
		openImage(img);
	}
}

function uniqArray(string, ss){
	if( matches(string, ".*#"+ss+"#.*") == 0) {
		string=string+dl+ss;
	}
//	print(string);
	ua=split(string, dl);
	return string;
}

function openImage(img){
	if ( os == "w" ){
		if (startsWith(img, LABDATADIR) == 1){
			//print(mountPoint);
			img=replace(img, LABDATADIR+"/", mountPoint);
		}
	} else {
		img=FILELIST[l];
	}
	
	if(File.exists(img)){
		if ( modus == "mp4" ||  modus == "clahe") {
			if(File.exists(img)){
				openMovie(img);
			}
		} else {
			print(img);
			if (displayFileList == 0){
				if(File.exists(img)){
					open(img);
				}
			}
		}
	} else {
		print("WARNING: "+img+" does not exist."); 
	}
}

function openMovie(path) {
	mplayer=getDirectory("imagej")+"../smplayer/smplayer.exe";
	movieRes=call("ij.Prefs.get", "tcf.disp."+tool+".movieRes","");
	fn=File.getName(path);
	open(getDirectory("plugins")+"ActionBar/icons/TCF/MPlayer.png");
	DID=getImageID();
	run("Canvas Size...", "width=1024 height=256 position=Center");
	rename(fn);
	string=split(fn,".");
	string=string[0];
	getDimensions(width, height, channels, slices, frames);
	fontSize=72;
	setForegroundColor(0, 0, 0);
	setFont("SansSerif", fontSize, "bold antialiased");
	drawString(string, 0, height/2+fontSize/2);
	run("Out [-]");
	run("Out [-]");
	run("Out [-]");
	setLocation(0,0);
	if (movieRes == "low"){
		if (matches(path, ".*720p.*") == 1){
			print(path);
			playMovie(path, DID);
		} 
	} else {
		if (matches(path, ".*720p.*") == 0){
			print(path);
			playMovie(path, DID);
		}
	}
	if (isOpen(DID)){
		selectImage(DID);
		close();
	}
}

function playMovie(path, DID){
	repeatMovie=call("ij.Prefs.get", "tcf.disp."+tool+".repeatMovie","");
	if (repeatMovie == 1){
		while (isOpen(DID)){
			exec("cmd", "/c", "start", mplayer, path);
		}
	} else {
		exec("cmd", "/c", "start", mplayer, path);
	}
}
