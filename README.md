# TPS-annobar 
The TPS-annobar is an extension of the [TPS-toolbar](https://gitlab.com/arnimjenett/TPS-toolbar). 

At the [TEFOR Core Facility](https://tcf.tefor.net/) the TPS-annobar is used to create and retrieve manual and automatic annotations within the ([fsdb](https://gitlab.com/arnimjenett/fsdb)), but it can also be applied to any arbitrary pool of images.  

## Installation
- download this repository (e.g. as .zip)
- unpack the downloaded data 
  - this will result in a folder 'Fiji.app', which contains the same structure as fiji comes with natively.
- shutdown all instances of the fiji you want to install the TPS-annobar to.
- copy-and-paste the new 'Fiji.app' folder on the 'fiji.app' folder of your local fiji-installation. 
- if you get warnings, that you may overwrite files, allow this, please.

-> When you restart the modified fiji, the TPS-toolbar will open up at startup. From there you can start the TPS-annobar by the 'ANNO'-button.

## Acknowledgement
This tool (as well as the [TPS-toolbar](https://gitlab.com/arnimjenett/TPS-toolbar) are based on [Jerome Mutterer](http://www.ibmp.cnrs.fr/annuaire/jerome-mutterer/)'s [ActionBar](https://imagejdocu.tudor.lu/doku.php?id=plugin:utilities:action_bar:start). 


